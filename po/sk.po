# Slovak translations for 0.3 package.
# Copyright (C) 2019 THE 0.3'S COPYRIGHT HOLDER
# This file is distributed under the same license as the 0.3 package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-10 15:26+0100\n"
"PO-Revision-Date: 2019-12-11 18:36+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: src/indicator.js:88
msgid "Bubblemail service not found !"
msgstr ""

#: src/indicator.js:88 src/indicator.js:90
msgid "Go to download page"
msgstr ""

#: src/indicator.js:89
msgid "Bubblemail service is not running !"
msgstr ""

#: src/indicator.js:89 src/indicator.js:91
msgid "Launch settings"
msgstr ""

#: src/indicator.js:90
msgid "Bubblemail service version mismatch, please upgrade !"
msgstr ""

#: src/indicator.js:91
msgid "Account error"
msgstr ""

#: src/indicator.js:128
msgid "Dismiss all"
msgstr ""

#: src/indicator.js:136
msgid "Refresh"
msgstr ""

#: src/indicator.js:146
msgid "Open mail app"
msgstr ""

#: src/indicator.js:154
msgid "Settings"
msgstr ""

#: src/indicator.js:158
msgid "Service Settings"
msgstr ""

#: src/indicator.js:162
msgid "Extension Settings"
msgstr ""

#: src/indicator.js:233
msgid "Auto refresh is disabled"
msgstr ""

#: src/indicator.js:420
msgid "Error on account "
msgstr ""

#: src/prefs.js:65
msgid "General"
msgstr ""

#: src/prefs.js:70
msgid "Show the indicator when maillist is empty"
msgstr ""

#: src/prefs.js:72
msgid "Animate mail count label"
msgstr ""

#: src/prefs.js:74
msgid "Show mail counter over the mail icon"
msgstr ""

#: src/prefs.js:76
msgid "Show menu icons"
msgstr ""

#: src/prefs.js:78
msgid "Show mail application launcher"
msgstr ""

#: src/prefs.js:83
msgid "Mail list"
msgstr ""

#: src/prefs.js:87
msgid "Show newest mails at first"
msgstr ""

#: src/prefs.js:89
msgid "Hide mail subjects when mail list exceeds"
msgstr ""

#: src/prefs.js:96
msgid "Subjects will be always visible if set to 0"
msgstr ""

#: src/prefs.js:100
msgid "Group mails by account"
msgstr ""

#: src/prefs.js:102
msgid "Show avatars"
msgstr ""

#: src/prefs.js:103
msgid "Show dates"
msgstr ""
