# Changelog
## [15] - 2021-11-10
 - Gnome 41 compatibility (f98ecfcc)
 - Fix crashes & bug on animation triggering (123c6da3)

## [14] - 2021-04-11
 - Fix bug: typo on webmail usage choice (0078d14f)

## [13] - 2021-04-11
 - Animate counter only on new mails (365f2434)
 - Option for a more conventional mail counter (31bc23a2) (#9)
 - Gnome 3.40 prefs window (gtk4) compatibility (cc597674, 3b65ab29, c7b3da12, ef238314) (#6)
 - Handle webmail usage (need service version > 1.3) (9e8bc0e7) (#7)
 - Fix meson build error (cd5ed06b)
 - Sync version numbering with gnome shell website (3b65ab29) (#8)

## [1.3] - 2020-10-17
 - Gnome 3.38 compatibility (11cacf35, b83ab95f, afd29adf) (#6)

## [1.2] - 2020-08-27
 - Adapt to new server error handling implementation (bfeac46f)
 - Add mailer menu item & options to disable it (05775608)
 - Add option to disable icons in menu (05775608)
 - Improve preferences window using frames (22085aa7)

## [1.1] - 2020-06-17
 - Adapt to new server error behavior

## [1.0] - 2020-05-24
 - Add counter label animation on changes with config option to disable it (6447662a, 55a20ac1, d6b8dbfc)
 - Use account status property to track errors (82b5eec7)
 - Improve submenu styles adding icons (fd9bbf9c, ab312708)
 - Fix bug: Download page button not working on version mismatch (816e4bb9)

## [0.71] - 2020-04-21
 - Fix bug: Preferences not working on Fedora 31 (e280f4e5)

## [0.7] - 2020-04-20
 - Improve main menu apparence : add min width and margins ()
 - Fix bug: error always shown when using unsecured account (15720020)
 - Fix bug (3.36.0): Extension settings doesn't open (c102c712)
 - Fix bug: Remove C language from project definition (c4a79faf)
 - Fix bug: indicator counter not refreshed on mail dismiss (29cd2c3a)

## [0.6] - 2020-03-16
 - Show dismiss button only on hover (32d1ed52)
 - Make indicator more compact (a84221b5)
 - Add 3.36 version compatibility
 - Code cleanup

## [0.51] - 2020-01-29
 - Fix bug: indicator bubble not always round
 - Fix regression: Fatal error on wayland

## [0.5] - 2020-01-29

 - New option to hide subject when the mail list exceeds specified value
 - Menu redesign
 - Indicator redesign
 - Make avatar size follow mail item size (ie reactive to theme and font size)
 - Make avatar always round
 - Massive code refactoring
 - Fix bug : indicator height don't fit screen (add scrollbar)
 - Code cleaning

## [0.4] - 2020-02-08

 - Fix bug : Dismiss specific mail not working (45a1f3a1)
 - Fix bug : Translation not working (46bdd426)
 - Code cleaning